from typing import List


class Game:
    arrangement = {}
    count = 0
    win_coordinates = ((0, 1, 2), (3, 4, 5), (6, 7, 8), (0, 3, 6), (1, 4, 7), (2, 5, 8), (0, 4, 8), (2, 4, 6))

    def __init__(self, arrangement: List[List]):
        i = 0
        for arrang in arrangement:
            for s in arrang:
                self.count += 1
                self.arrangement[i] = s
                i += 1

    def check_game(self):
        if self.count == 0:
            return 'Игра только началась'
        else:
            for each in self.win_coordinates:
                if self.arrangement.get(each[0], None) == 'X' and self.arrangement.get(each[1], None) == 'X' and \
                        self.arrangement.get(each[2], None) == 'X':
                    return 'Победили крестики'
                elif self.arrangement.get(each[0], None) == '0' and self.arrangement.get(each[1], None) == '0' and \
                        self.arrangement.get(each[2], None) == '0':
                    return 'Победили нолики'
            if self.count != 9:
                return 'Игра продолжается'
            else:
                return 'Игра завершилась ничьей'


if __name__ == '__main__':
    example_arrangement = [
        ['X', 'X', '0'],
        ['X', '0', '0'],
        ['0', 'X', ],
    ]
    game = Game(example_arrangement)
    game_result = game.check_game()
    print(game_result)
