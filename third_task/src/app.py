import asyncio

from openweather import get_data_by_city
from write_file import write_file


async def main():
    cities = ['Sochi', 'Almaty', 'New York']
    result = dict()
    for city in cities:
        result[city] = await get_data_by_city(city)
    await write_file(result)


loop = asyncio.get_event_loop()
loop.run_until_complete(main())
