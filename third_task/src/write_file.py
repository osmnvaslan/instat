import json


async def write_file(data: dict) -> None:

    # Serialize data into file:
    json.dump(data, open("weather_data.json", 'w'))

    # Read data from file:
    data = json.load(open("weather_data.json"))
    print(data)
