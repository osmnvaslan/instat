from aiohttp import ClientSession


async def get_data_by_city(city: str) -> dict:
    async with ClientSession() as session:
        url = f'http://api.openweathermap.org/data/2.5/weather'
        params = {'q': city, 'APPID': '2a4ff86f9aaa70041ec8e82db64abf56'}
        async with session.get(url=url, params=params) as response:
            weather_json = await response.json()
            try:
                return weather_json
            except KeyError:
                return {}
